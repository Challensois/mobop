package ch.hes_so.mobop;

import android.support.design.widget.FloatingActionButton;
import android.test.ActivityInstrumentationTestCase2;

public class DailyActivitiesTest extends ActivityInstrumentationTestCase2<DailyActivities> {

    private DailyActivities mFirstTestActivity;
    FloatingActionButton mFirstTestText;

    public DailyActivitiesTest() {
        super(DailyActivities.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mFirstTestActivity = getActivity();
        mFirstTestText = (FloatingActionButton) mFirstTestActivity
                        .findViewById(R.id.btn_add_activity_block);
    }

    public void testPreconditions() {
        assertNotNull("mFirstTestActivity is null", mFirstTestActivity);
        assertNotNull("mFirstTestText is null", mFirstTestText);
    }
}