package ch.hes_so.mobop;

import android.os.Bundle;
import android.view.MenuItem;

import ch.hes_so.mobop.model.ActivityList;

public class SetActivitiesActivity extends TimeManagementActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_fragment);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment, new SetActivitiesFragment())
                    .commit();
        }
    }

    @Override
    protected void onStop() {
        ActivityList.saveActivityList(this);
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        SetActivitiesFragment fragment = (SetActivitiesFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment);
        if(fragment != null && !fragment.removeEditItem()) {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                final SetActivitiesFragment fragment = (SetActivitiesFragment)
                        getSupportFragmentManager().findFragmentById(R.id.fragment);
                if(fragment != null && fragment.isEditModeOn()) {
                    askConfirmation();
                    return true;
                } else {
                    return super.onNavigateUp();
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void scrollToPositionWithOffset(int position, int offset) {
        SetActivitiesFragment fragment = (SetActivitiesFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment);
        if(fragment != null) {
            fragment.scrollToPositionWithOffset(position, offset);
        }
    }

    private void askConfirmation() {
        SetActivitiesFragment fragment = (SetActivitiesFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment);
        if(fragment != null) {
            fragment.askConfirmation(this);
        }
    }
}
