package ch.hes_so.mobop;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import ch.hes_so.mobop.model.ActivityEditorAdapter;
import ch.hes_so.mobop.model.ActivityList;
import ch.hes_so.mobop.model.Category;

public class SetActivitiesFragment extends Fragment {
    private ActivityEditorAdapter activityEditorAdapter;
    private LinearLayoutManager mLayoutManager;

    private final static String POSITION_EDITED_ITEM = "position";
    private final static String VALUE_EDITED_ITEM = "value";
    private final static String CONFIRMATION_DIALOG = "dialog";
    private boolean confirmationAsked = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_set_activities, container, false);

        FragmentActivity parent = getActivity();
        activityEditorAdapter = new ActivityEditorAdapter(parent);

        for(Category category : Category.values()) {
            activityEditorAdapter.addSectionHeaderItem(category.toString());
            List<String> activityList = ActivityList.getActivityListForCategory(parent, category);
            for(String activityName : activityList) {
                activityEditorAdapter.addItem(activityName);
            }
        }

        RecyclerView mRecyclerView = (RecyclerView) rootView.findViewById(R.id.list_activities);
        mRecyclerView.setAdapter(activityEditorAdapter);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(parent,
                LinearLayout.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        registerForContextMenu(mRecyclerView);
        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(CONFIRMATION_DIALOG, confirmationAsked);
        if(activityEditorAdapter.isEditModeOn()) {
            outState.putInt(POSITION_EDITED_ITEM, activityEditorAdapter.getEditItem());
            outState.putString(VALUE_EDITED_ITEM, activityEditorAdapter.getEditItemValue());
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState != null) {
            if (savedInstanceState.containsKey(POSITION_EDITED_ITEM)
                    && savedInstanceState.containsKey(VALUE_EDITED_ITEM)) {
                activityEditorAdapter.setEditItem(savedInstanceState.getInt(POSITION_EDITED_ITEM),
                        savedInstanceState.getString(VALUE_EDITED_ITEM));
            }
            if(savedInstanceState.getBoolean(CONFIRMATION_DIALOG, false)) {
                askConfirmation((SetActivitiesActivity) getActivity());
            }
        }
    }

    public boolean removeEditItem() {
        return activityEditorAdapter.removeEditItem();
    }

    public boolean isEditModeOn() {
        return activityEditorAdapter.isEditModeOn();
    }

    public void scrollToPositionWithOffset(int position, int offset) {
        mLayoutManager.scrollToPositionWithOffset(position, offset);
    }

    public void askConfirmation(final SetActivitiesActivity setActivitiesActivity) {
        confirmationAsked = true;
        AlertDialog.Builder builder = new AlertDialog.Builder(setActivitiesActivity);
        builder.setTitle("Warning!")
                .setMessage("It seems that you are currently editing a new activity!" +
                        "\nAre your sure you want to quit ? You will lose all modifications!")
                .setPositiveButton("Quit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        removeEditItem();
                        setActivitiesActivity.onNavigateUp();
                    }
                })
                .setNegativeButton("Stay", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        confirmationAsked = false;
                    }
                }).create().show();
    }
}
