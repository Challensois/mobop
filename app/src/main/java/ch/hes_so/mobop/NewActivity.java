package ch.hes_so.mobop;

import android.os.Bundle;

import ch.hes_so.mobop.model.ActivitySelection;
import ch.hes_so.mobop.model.ActivitySelectionList;
import ch.hes_so.mobop.model.Category;

public class NewActivity extends TimeManagementActivity {

    private final static String POSITION = "POSITION";
    private final static String CATEGORIES = "CATEGORIES";
    private final static String ACTIVITY_NAMES = "ACTIVITY_NAMES";
    private final static String SELECTIONS = "SELECTIONS";
    private ActivitySelectionList activities;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(activities == null) {
            activities = new ActivitySelectionList();
        }
        setContentView(R.layout.activity_with_fragment);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment, new NewActivityFragment())
                    .commit();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        int size = activities.getSize();
        int[] categories = new int[size];
        String[] activity_names = new String[size];
        boolean[] selections = new boolean[size];

        for(int i = 0; i < activities.getSize(); i++) {
            ActivitySelection temp = activities.get(i);
            categories[i] = temp.getCategory().ordinal();
            activity_names[i] = temp.getName();
            selections[i] = temp.isSelected();
        }
        outState.putIntArray(CATEGORIES, categories);
        outState.putStringArray(ACTIVITY_NAMES, activity_names);
        outState.putBooleanArray(SELECTIONS, selections);
        NewActivityFragment newActivityFragment = (NewActivityFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment);
        if (newActivityFragment != null) {
            outState.putInt(POSITION, newActivityFragment.getSelectedTabPosition());
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        activities = new ActivitySelectionList();
        int[] categories = savedInstanceState.getIntArray(CATEGORIES);
        String[] activity_names = savedInstanceState.getStringArray(ACTIVITY_NAMES);
        boolean[] selections = savedInstanceState.getBooleanArray(SELECTIONS);
        if(categories != null && activity_names != null && selections != null) {
            for(int i = 0; i < categories.length; i++) {
                ActivitySelection temp = new ActivitySelection(Category.values()[categories[i]],
                        activity_names[i]);
                temp.setSelected(selections[i]);
                activities.add(temp);
            }
        }
        NewActivityFragment newActivityFragment = (NewActivityFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment);
        if (newActivityFragment != null) {
            newActivityFragment.setCurrentItem(savedInstanceState.getInt(POSITION));
        }
    }

    @Override
    public void onBackPressed() {
        boolean foundOne = false;
        for (ActivitySelection as : activities) {
            if(as.isSelected()) {
                as.setSelected(false);
                foundOne = true;
                break;
            }
        }
        if(!foundOne) {
            super.onBackPressed();
        }
    }

    public ActivitySelection addActivity(Category category, String name) {
        ActivitySelection activitySelection = new ActivitySelection(category, name);
        if(activities == null) {
            activities = new ActivitySelectionList();
        }
        for(ActivitySelection as : activities) {
            if(as.equals(activitySelection)) {
                return as;
            }
        }
        activities.add(activitySelection);
        return activitySelection;
    }

    public ActivitySelectionList getActivities() {
        return activities;
    }
}
