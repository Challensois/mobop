package ch.hes_so.mobop;

import android.support.v7.app.AppCompatActivity;

import ch.hes_so.mobop.model.Category;

public class TimeManagementActivity extends AppCompatActivity {
    public int getImage(Category category) {
        switch (category) {
            case UNCATEGORIZED:
                return R.raw.uncategorized;
            case PERSONAL:
                return R.raw.personal;
            case WORK:
                return R.raw.work;
            case SPORT:
                return R.raw.sport;
            case TRANSPORT:
                return R.raw.transport;
            default:
                throw new RuntimeException();
        }
    }

    public int getImage(String categoryName) {
        for(Category category : Category.values()) {
            if(category.toString().equals(categoryName)) {
                return getImage(category);
            }
        }
        throw new RuntimeException("Category not found!");
    }
}
