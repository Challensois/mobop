package ch.hes_so.mobop;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ch.hes_so.mobop.model.ActivityList;
import ch.hes_so.mobop.model.ActivitySelection;
import ch.hes_so.mobop.model.ActivitySelectorAdapter;
import ch.hes_so.mobop.model.Category;

public class ActivityListFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";

    private ArrayList<ActivitySelection> activityList;

    public static ActivityListFragment newInstance(int page) {
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, page);
        ActivityListFragment fragment = new ActivityListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Category category = Category.values()[getArguments().getInt(ARG_PAGE)];
        List<String> activitySet = ActivityList.getActivityListForCategory(getContext(), category);
        activityList = new ArrayList<>();
        for(int i = 0; i < activitySet.size(); i++) {
            ActivitySelection as = ((NewActivity) getActivity()).addActivity(category, activitySet.get(i));
            activityList.add(as);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;
        if(!activityList.isEmpty()) {
            view = inflater.inflate(R.layout.fragment_activity_name_page, container, false);
            RecyclerView mRecyclerView = (RecyclerView) view;

            ActivitySelectorAdapter mActivitiesAdapter = new ActivitySelectorAdapter(
                    (NewActivity) getActivity(), activityList);

            mRecyclerView.setHasFixedSize(true);
            mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));

            // use a linear layout manager
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            mRecyclerView.setLayoutManager(mLayoutManager);

            // specify an adapter (see also next example)
            mRecyclerView.setAdapter(mActivitiesAdapter);
            registerForContextMenu(mRecyclerView);
        } else {
            view = inflater.inflate(R.layout.fragment_activity_name_page_empty, container, false);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), SetActivitiesActivity.class);
                    startActivity(intent);
                }
            });
        }
        return view;
    }

    class DividerItemDecoration extends RecyclerView.ItemDecoration {

        private final int[] ATTRS = new int[]{android.R.attr.listDivider};

        private Drawable mDivider;

        public DividerItemDecoration(Context context) {
            final TypedArray styledAttributes = context.obtainStyledAttributes(ATTRS);
            mDivider = styledAttributes.getDrawable(0);
            styledAttributes.recycle();
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            int left = parent.getPaddingLeft();
            int right = parent.getWidth() - parent.getPaddingRight();

            int childCount = parent.getChildCount();
            for (int i = 0; i < childCount - 1; i++) {
                View child = parent.getChildAt(i);

                RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

                int top = child.getBottom() + params.bottomMargin;
                int bottom = top + mDivider.getIntrinsicHeight();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }
    }
}
