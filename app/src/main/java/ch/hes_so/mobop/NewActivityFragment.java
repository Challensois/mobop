package ch.hes_so.mobop;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Observable;
import java.util.Observer;

import ch.hes_so.mobop.model.ActivityBlock;
import ch.hes_so.mobop.model.ActivityList;
import ch.hes_so.mobop.model.ActivitySelection;
import ch.hes_so.mobop.model.ActivitySelectionList;
import ch.hes_so.mobop.model.Category;
import ch.hes_so.mobop.model.Task;
import ch.hes_so.mobop.model.Timeline;

public class NewActivityFragment extends Fragment implements Observer {
    private Task task;
    private ActivityBlock oldActivityBlock;
    private ActivityBlock activityBlock;
    private boolean setBegin = false;
    private boolean setEnd = false;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView txtFrom;
    private TextView txtTo;
    private TimePickerDialog timePicker;
    private Button btnAdd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Intent intent = getActivity().getIntent();
        View rootView = inflater.inflate(R.layout.fragment_new_activity_block, container, false);

        NewActivity context = (NewActivity) getActivity();
        context.getActivities().addObserver(this);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        viewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        ActivityNamePagerAdapter adapter = new ActivityNamePagerAdapter(
                context.getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        // Give the TabLayout the ViewPager
        tabLayout = (TabLayout) rootView.findViewById(R.id.sliding_tabs);
        tabLayout.setupWithViewPager(viewPager);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if(tab != null) {
                tab.setIcon(context.getImage(Category.values()[i]));
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActionBar ab = context.getSupportActionBar();
            if(ab != null) {
                ab.setElevation(0);
                tabLayout.setElevation(4);
            }
        }

        txtFrom = (TextView) rootView.findViewById(R.id.txt_from);
        txtTo = (TextView) rootView.findViewById(R.id.txt_to);
        btnAdd = (Button) rootView.findViewById(R.id.btn_confirm_add_activity_block);

        txtFrom.setText(Task.getMinStartingTime());
        txtTo.setText(Task.getMaxStoppingTime());
        activityBlock = new ActivityBlock(Task.getNewDefaultTask(),
                txtFrom.getText().toString(), txtTo.getText().toString());

        final TimePickerDialog.OnTimeSetListener timePickerListener =
                new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int selectedHour,
                                          int selectedMinute) {
                        if(setBegin) {
                            setFromTime(selectedHour, selectedMinute);
                        } else if (setEnd) {
                            setToTime(selectedHour, selectedMinute);
                        } else {
                            Log.e("TimePicker", "Invalid State...");
                        }
                    }
                };

        txtFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBegin = true;
                timePicker = new TimePickerDialog(getActivity(),
                        timePickerListener,
                        activityBlock.getStartHour(),
                        activityBlock.getStartMinute(),
                        true);
                timePicker.show();
            }
        });

        txtTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setEnd = true;
                timePicker = new TimePickerDialog(getActivity(),
                        timePickerListener,
                        activityBlock.getEndHour(),
                        activityBlock.getEndMinute(),
                        true);
                timePicker.show();
            }
        });

        btnAdd.setVisibility(View.INVISIBLE);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkActivityBlock()) {
                    activityBlock.setTask(task);
                    activityBlock.setBegin(txtFrom.getText().toString());
                    activityBlock.setEnd(txtTo.getText().toString());
                    Timeline.getInstance().addActivity(activityBlock);
                    navigateBack(Activity.RESULT_OK);
                }
            }
        });

        if (intent.hasExtra("activity_index_in_timeline"))
            populateWindow(intent);

        return rootView;
    }

    private void populateWindow(Intent intent) {
        final int index = intent.getIntExtra("activity_index_in_timeline", 0);
        oldActivityBlock = Timeline.getInstance().getActivities().get(index);
        Task oldTask = oldActivityBlock.getTask();
        activityBlock = new ActivityBlock(oldTask,
                oldActivityBlock.getBegin(), oldActivityBlock.getEnd());
        setLayoutSelectedActivity(oldTask.getCategory(), oldTask.getName());

        setFromTime(activityBlock.getStartHour(), activityBlock.getStartMinute());
        setToTime(activityBlock.getEndHour(), activityBlock.getEndMinute());

        getActivity().setTitle("Edit Activity");
        btnAdd.setText(getActivity().getResources().getString(R.string.btn_edit_activity_block));
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityBlock.setTask(task);
                activityBlock.setBegin(txtFrom.getText().toString());
                activityBlock.setEnd(txtTo.getText().toString());
                if (!oldActivityBlock.equals(activityBlock)) {
                    if (checkActivityBlock()) {
                        Timeline.getInstance().removeActivity(index);
                        Timeline.getInstance().addActivity(activityBlock);
                        navigateBack(Activity.RESULT_OK);
                    }
                } else {
                    Log.d("EditActivity", "You didn't change any parameters... Nothing to do.");
                    navigateBack(Activity.RESULT_FIRST_USER);
                }
            }
        });
    }

    private void setFromTime(int selectedHour, int selectedMinute) {
        activityBlock.setBegin(selectedHour, selectedMinute);
        txtFrom.setText(String.format("%02d", selectedHour) + ":"
                + String.format("%02d", selectedMinute));
        setBegin = false;
    }

    private void setToTime(int selectedHour, int selectedMinute) {
        activityBlock.setEnd(selectedHour, selectedMinute);
        txtTo.setText(String.format("%02d", selectedHour) + ":"
                + String.format("%02d", selectedMinute));
        setEnd = false;
    }

    private boolean checkActivityBlock() {
        boolean ok = false;
        try {
            ok = activityBlock.checkValid();
        } catch (RuntimeException e) {
            String message = e.getMessage();
            if (message.equals(ActivityBlock.getLengthExceptionString())) {
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                int tempH = activityBlock.getStartHour();
                                int tempM = activityBlock.getStartMinute();
                                setFromTime(activityBlock.getEndHour(), activityBlock.getEndMinute());
                                setToTime(tempH, tempM);
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                //No button clicked
                                break;
                        }
                    }
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Maybe the \"From\" time and the \"To\" time are reversed. Would you like to swap them ?")
                        .setPositiveButton("Yes", dialogClickListener).setIcon(android.R.drawable.ic_dialog_alert)
                        .setNegativeButton("No", dialogClickListener).setCancelable(false)
                        .setTitle(message).show();
            } else if (message.equals(ActivityBlock.getNameExceptionString()) || message.equals(ActivityBlock.getZeroLengthExceptionString())) {
                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
            } else {
                throw e;
            }
        }
        return ok;
    }

    private void navigateBack(int result) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", result);
        getActivity().setResult(result, returnIntent);
        getActivity().finish();
    }

    public int getSelectedTabPosition() {
        int selectedTab = 0;
        if(tabLayout != null) {
            selectedTab = tabLayout.getSelectedTabPosition();
        }
        return selectedTab;
    }

    public void setCurrentItem(int itemID) {
        if(viewPager != null) {
            viewPager.setCurrentItem(itemID);
        }
    }

    public void setLayoutSelectedActivity(Category category, String activity) {
        viewPager.setCurrentItem(category.ordinal());
        task = new Task(category, activity);
        ActivitySelection activitySelection = new ActivitySelection(category, activity);
        ActivitySelectionList asl = ((NewActivity) getActivity()).getActivities();
        if(asl.getSize() > 0) {
            boolean found = false;
            for (ActivitySelection as : asl) {
                if (as.equals(activitySelection)) {
                    found = true;
                    as.setSelected(true);
                    break;
                }
            }
            if (!found) {
                throw new RuntimeException("Not found");
            }
        } else {
            if(ActivityList.getActivityListForCategory(getContext(), category).contains(activity)) {
                activitySelection.setSelected(true);
                asl.add(activitySelection);
            } else {
                Toast.makeText(getActivity(), "The activity \"" + activity + "\" doesn't exist " +
                        "anymore!\nYou can still edit it if you want...", Toast.LENGTH_LONG).show();
            }
        }
        setLayoutSelectedActivity();
    }

    public void setLayoutSelectedActivity() {
        btnAdd.setVisibility(View.VISIBLE);
    }

    public boolean resetLayoutSelectedActivity() {
        if(task != null) {
            task = null;
            btnAdd.setVisibility(View.INVISIBLE);
            return true;
        }
        return false;
    }

    @Override
    public void update(Observable observable, Object data) {
        if(observable.getClass() == ActivitySelectionList.class) {
            ActivitySelectionList asl = ((ActivitySelectionList)observable);
            boolean foundOne = false;
            for(ActivitySelection as : asl) {
                if(as.isSelected()) {
                    setLayoutSelectedActivity();
                    foundOne = true;
                    task = new Task(as.getCategory(), as.getName());
                }
            }
            if(!foundOne) {
                resetLayoutSelectedActivity();
            }
        }
    }
}