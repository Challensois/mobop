package ch.hes_so.mobop;

import android.os.Bundle;

import java.util.Date;

import ch.hes_so.mobop.model.TimelineSaver;

public class StatisticsActivity extends TimeManagementActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_fragment);

        if (savedInstanceState == null) {
            StatisticsFragment fragment = new StatisticsFragment();
            int count = getIntent().getIntExtra(TimelineSaver.INTENT_FILENAME, 0);
            String[] dates = new String[count];
            for(int i = 0; i < count; i++) {
                Date date = (Date) getIntent().getSerializableExtra(TimelineSaver.INTENT_FILENAME
                    + i);
                dates[i] = TimelineSaver.getFileNameFromDate(date);
            }
            if(count > 0) {
                Bundle args = new Bundle();
                args.putStringArray(TimelineSaver.INTENT_FILENAME, dates);
                fragment.setArguments(args);
            }
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment, fragment)
                    .commit();
        }
    }

    @Override
    public void onBackPressed() {
        StatisticsFragment fragment = (StatisticsFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment);
        if(fragment != null) {
            if(!fragment.goBackToCategories()) {
                super.onBackPressed();
            }
        }
    }
}
