package ch.hes_so.mobop;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.sql.Time;
import java.util.Calendar;

import ch.hes_so.mobop.model.ActivityBlock;
import ch.hes_so.mobop.model.Category;
import ch.hes_so.mobop.model.Task;
import ch.hes_so.mobop.model.Timeline;
import ch.hes_so.mobop.model.ActivityBlockAdapter;
import ch.hes_so.mobop.model.TimelineSaver;

public class DailyActivitiesFragment extends Fragment {

    private FloatingActionButton addActivity;

    public final static int NEW_ACTIVITY_CODE = 666;

    private final static String SELECTED_OUT_STRING = "selected";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_daily_activities, container, false);

        final DailyActivities parent = (DailyActivities) getActivity();

        Calendar calendar = Calendar.getInstance();
        String fileName = TimelineSaver.getFileNameFromDay(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        ActivityBlockAdapter mActivitiesAdapter;
        if(getActivity().getBaseContext().getFileStreamPath(fileName).exists()) {
            mActivitiesAdapter = new ActivityBlockAdapter(parent,
                    Timeline.readFromFile(getContext(), fileName));
        } else {
            mActivitiesAdapter = new ActivityBlockAdapter(parent);
        }

        // Get a reference to the ListView, and attach this adapter to it.
        RecyclerView mRecyclerView = (RecyclerView) rootView.findViewById(R.id.list_activity_blocks);

        mRecyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(parent);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mRecyclerView.setAdapter(mActivitiesAdapter);
        registerForContextMenu(mRecyclerView);

        addActivity = (FloatingActionButton) rootView.findViewById(R.id.btn_add_activity_block);
        addActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(parent, NewActivity.class);
                startActivityForResult(intent, NEW_ACTIVITY_CODE);
            }
        });

        //Uncomment if you need to load sample data
        //loadSampleData();

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        int index = 0;
        for(ActivityBlock ab : Timeline.getInstance()) {
            outState.putBoolean(SELECTED_OUT_STRING + index, ab.isSelected());
            index += 1;
        }
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState != null) {
            Timeline timeline = Timeline.getInstance();
            if(timeline != null) {
                for (int i = 0; i < timeline.getActivities().size();i++) {
                    if(savedInstanceState.getBoolean(SELECTED_OUT_STRING + i, false)) {
                        timeline.setSelected(i);
                    }
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("ActivityResults", "received code " + requestCode + " with result code " + resultCode);
        // Check which request we're responding to
        if (requestCode == NEW_ACTIVITY_CODE) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    break;
                case Activity.RESULT_FIRST_USER:
                    Toast.makeText(getActivity(),
                            "You didn't change any parameters!",
                            Toast.LENGTH_SHORT).show();
                    break;
                case Activity.RESULT_CANCELED:
                    break;
            }
        }
    }

/* Uncomment if you need to loadSampleData
    public void loadSampleData() {
        Calendar calendar = Calendar.getInstance();
        int initialDay = calendar.get(Calendar.DAY_OF_YEAR);
        Timeline timeline = Timeline.getInstance();
        for (int i = initialDay - 7; i < initialDay; i++) {
            calendar.set(Calendar.DAY_OF_YEAR, i);
            // Resets timeline
            timeline.addActivity(new ActivityBlock(Task.getDefaultTask(),
                    Task.getMinStartingTime(), Task.getMaxStoppingTime()));
            switch(i - (initialDay - 7)) {
                case 0:
                    timeline.addActivity(new ActivityBlock(new Task(Category.SPORT, "Rugby"),
                            Task.getMinStartingTime(), Task.getMaxStoppingTime()));
                    Log.d("i", "0");
                    break;
                case 1:
                    timeline.addActivity(new ActivityBlock(new Task(Category.PERSONAL, "Eat"),
                            "12:00", "13:00"));
                    Log.d("i", "1");
                    break;
                case 2:
                    timeline.addActivity(new ActivityBlock(new Task(Category.WORK, "Work"),
                            "08:00", "12:00"));
                    timeline.addActivity(new ActivityBlock(new Task(Category.WORK, "Work"),
                            "13:00", "17:00"));
                    Log.d("i", "2");
                    break;
                case 3:
                    timeline.addActivity(new ActivityBlock(new Task(Category.TRANSPORT, "Train"),
                            "07:00", "07:30"));
                    timeline.addActivity(new ActivityBlock(new Task(Category.TRANSPORT, "Train"),
                            "17:30", "18:00"));
                    Log.d("i", "3");
                    break;
                case 4:
                    timeline.addActivity(new ActivityBlock(new Task(Category.TRANSPORT, "Train"),
                            "07:00", "07:30"));
                    timeline.addActivity(new ActivityBlock(new Task(Category.WORK, "Work"),
                            "08:00", "12:00"));
                    timeline.addActivity(new ActivityBlock(new Task(Category.PERSONAL, "Eat"),
                            "12:00", "13:00"));
                    timeline.addActivity(new ActivityBlock(new Task(Category.WORK, "Work"),
                            "13:00", "17:00"));
                    timeline.addActivity(new ActivityBlock(new Task(Category.TRANSPORT, "Train"),
                            "17:30", "18:00"));
                    Log.d("i", "4");
                    break;
                case 5:
                    timeline.addActivity(new ActivityBlock(new Task(Category.TRANSPORT, "Train"),
                            "17:30", "18:00"));
                    timeline.addActivity(new ActivityBlock(new Task(Category.SPORT, "Rugby"),
                            "18:30", "23:00"));
                    Log.d("i", "5");
                    break;
                case 6:
                    timeline.addActivity(new ActivityBlock(new Task(Category.SPORT, "Rugby"),
                            "10:30", "15:00"));
                    timeline.addActivity(new ActivityBlock(new Task(Category.SPORT, "Foot"),
                            "16:30", "23:00"));
                    Log.d("i", "6");
                    break;
                default:
                    Log.d("i", "should not happen");
            }
            Timeline.getInstance().saveToFile((DailyActivities) getActivity(), calendar);
        }
        timeline.addActivity(new ActivityBlock(Task.getDefaultTask(),
                Task.getMinStartingTime(), Task.getMaxStoppingTime()));
    }
*/

    public void setAddButtonVisibility(boolean visible) {
        if(addActivity != null) {
            addActivity.setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    public void clickEvent(int position) {
        Intent intent = new Intent(getActivity(), NewActivity.class)
                .putExtra("activity_index_in_timeline", position);
        startActivityForResult(intent, DailyActivitiesFragment.NEW_ACTIVITY_CODE);
    }
}
