package ch.hes_so.mobop;

import android.os.Bundle;

public class CalendarActivity extends TimeManagementActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_with_fragment);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment, new CalendarFragment())
                    .commit();
        }
    }
}
