package ch.hes_so.mobop;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ch.hes_so.mobop.model.Category;

public class ActivityNamePagerAdapter extends FragmentPagerAdapter {
    private final int PAGE_COUNT;
    private final String tabTitles[];

    public ActivityNamePagerAdapter(FragmentManager fm) {
        super(fm);
        Category[] values = Category.values();
        tabTitles = new String[values.length];
        for(int i = 0; i < tabTitles.length; i++) {
            tabTitles[i] = values[i].toString();
        }
        PAGE_COUNT = tabTitles.length;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        return ActivityListFragment.newInstance(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}