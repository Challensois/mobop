package ch.hes_so.mobop;

import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.Switch;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ch.hes_so.mobop.model.ActivityBlock;
import ch.hes_so.mobop.model.Category;
import ch.hes_so.mobop.model.Timeline;
import ch.hes_so.mobop.model.TimelineSaver;

public class StatisticsFragment extends Fragment implements OnChartValueSelectedListener {
    private final static int SHOW_CATEGORIES = 0;
    private final static int SHOW_ACTIVITIES = 1;
    private int mode;

    private final static int PIE_CHART = 0;
    private final static int BAR_CHART = 1;
    private final static int SPIDER_CHART = 2;
    private int visibleChart;

    private String mainTitle;
    private DaysToCategoriesMap daysToCategoriesMap;
    private ArrayList<String> datasetTitles;

    private Switch switchUncategorized;
    private PieChart pieChart;
    private BarChart barChart;
    private RadarChart spiderChart;
    private LineChart lineChart;
    private RadioButton radioButtonSpider;

    private ArrayList<Integer> colors;
    private ArrayList<Integer> colorActivities;
    private TextView txtNoData;
    private MyValueFormatter mValueFormatter;
    private Category filterCategory;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ArrayList<Timeline> timelines = new ArrayList<>();
        String[] filenames;
        Bundle bundle = getArguments();
        if(bundle == null) {
            mainTitle = "current day";
            timelines.add(Timeline.getInstance());
            filenames = new String[1];
            filenames[0] = mainTitle;
        } else {
            String[] tempFilenames = bundle.getStringArray(TimelineSaver.INTENT_FILENAME);
            if(tempFilenames != null) {
                filenames = new String[tempFilenames.length];
                for (int i = 0; i < tempFilenames.length; i++) {
                    Timeline timeline = Timeline.readFromFile(getContext(), tempFilenames[i]);
                    Log.d(i + tempFilenames[i], timeline.toString());
                    timelines.add(timeline);
                    String[] yyMmDdFromFileName = TimelineSaver
                            .getYY_MM_DD_FromFileName(tempFilenames[i]);
                    filenames[i] = yyMmDdFromFileName[0] + "." + (Integer
                            .valueOf(yyMmDdFromFileName[1]) + 1) + "." + yyMmDdFromFileName[2];
                }
                mainTitle = getResources().getQuantityString(R.plurals.days, filenames.length,
                        filenames.length);
            } else {
                throw new RuntimeException("Didn't find " + TimelineSaver.INTENT_FILENAME +
                " parameter in the bundle!");
            }
        }
        initData(filenames, timelines);
        mValueFormatter = new MyValueFormatter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_statistics, container, false);

        TextView titleView = (TextView) rootView.findViewById(R.id.title_statistics);
        Resources res = getResources();
        titleView.setText(String.format(res.getString(R.string.statistics_title), mainTitle));

        txtNoData = (TextView) rootView.findViewById(R.id.txt_no_data);

        pieChart = (PieChart) rootView.findViewById(R.id.pie_chart);
        pieChart.setDescription("");
        pieChart.setHoleColorTransparent(true);
        pieChart.setCenterTextSize(18);
        pieChart.setCenterTextColor(ContextCompat.getColor(getContext(),
                android.R.color.tertiary_text_light));
        pieChart.animateY(1500);
        pieChart.getLegend().setEnabled(false);
        pieChart.setOnChartValueSelectedListener(this);

        barChart = (BarChart) rootView.findViewById(R.id.bar_chart);
        barChart.setNoDataText(getString(R.string.no_data_graph));
        barChart.getXAxis().setTextSize(4);
        barChart.animateXY(1000, 1000);
        barChart.setDescriptionTextSize(14);
        barChart.setDescriptionColor(ContextCompat.getColor(getContext(),
                android.R.color.tertiary_text_light));
        barChart.setDrawGridBackground(false);
        barChart.setOnChartValueSelectedListener(this);

        Legend l = barChart.getLegend();
        l.setPosition(Legend.LegendPosition.BELOW_CHART_RIGHT);

        updateDescriptions(getString(R.string.graph_legend_categories));

        spiderChart = (RadarChart) rootView.findViewById(R.id.spider_chart);
        spiderChart.setDescription("");

        lineChart = (LineChart) rootView.findViewById(R.id.line_chart);
        lineChart.setDescription("");

        colors = new ArrayList<>();
        colorActivities = new ArrayList<>();

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);
        for (int c : ColorTemplate.COLORFUL_COLORS) {
            colorActivities.add(c);
            colors.add(c);
        }
        for (int c : ColorTemplate.VORDIPLOM_COLORS) {
            colorActivities.add(c);
            colors.add(c);
        }
        for (int c : ColorTemplate.LIBERTY_COLORS) {
            colorActivities.add(c);
            colors.add(c);
        }
        for (int c : ColorTemplate.JOYFUL_COLORS) {
            colorActivities.add(c);
            colors.add(c);
        }

        switchUncategorized = (Switch) rootView.findViewById(R.id.switch_uncategorized);
        checkUncategorized();
        switchUncategorized.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshData();
            }
        });

        RadioButton radioButtonPie = (RadioButton) rootView.findViewById(R.id.radio_pie);
        RadioButton radioButtonBar = (RadioButton) rootView.findViewById(R.id.radio_bar);
        radioButtonSpider = (RadioButton) rootView.findViewById(R.id.radio_spider);

        radioButtonPie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibleChart(PIE_CHART, true);
            }
        });

        radioButtonBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibleChart(BAR_CHART, true);
            }
        });

        radioButtonSpider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setVisibleChart(SPIDER_CHART, true);
            }
        });

        if(daysToCategoriesMap.entrySet().size() > 1) {
            radioButtonSpider.performClick();
        }

        setVisibleChart(visibleChart, true);
        switchMode(SHOW_CATEGORIES, null);

        return rootView;
    }

    private void checkUncategorized() {
        if(daysToCategoriesMap.joinAll().get(Category.UNCATEGORIZED).getTotalDuration() == 0) {
            switchUncategorized.setEnabled(false);
        }
    }

    private void switchMode(int newMode, Category filterCategory) {
        Log.d("Mode", "Switched from " + mode + " to " + newMode);
        mode = newMode;
        switch(mode) {
            case SHOW_CATEGORIES:
                radioButtonSpider.setText(getString(R.string.spider_chart));
                radioButtonSpider.setEnabled(true);
                if(visibleChart == SPIDER_CHART) {
                    lineChart.setVisibility(View.GONE);
                    spiderChart.setVisibility(View.VISIBLE);
                }
                assert filterCategory == null;
                this.filterCategory = null;
                updateDescriptions(getString(R.string.graph_legend_categories));
                switchUncategorized.setVisibility(View.VISIBLE);
                break;
            case SHOW_ACTIVITIES:
                radioButtonSpider.setText(getString(R.string.line_chart));
                if(daysToCategoriesMap.size() == 1) {
                    radioButtonSpider.setEnabled(false);
                } else {
                    radioButtonSpider.setEnabled(true);
                }
                if(visibleChart == SPIDER_CHART) {
                    throw new RuntimeException("Should not happen!");
                    /*
                    lineChart.setVisibility(View.VISIBLE);
                    spiderChart.setVisibility(View.GONE);
                    */
                }
                assert filterCategory != null;
                this.filterCategory = filterCategory;
                updateDescriptions(String.format(getString(R.string.graph_legend_activities),
                        filterCategory));
                switchUncategorized.setVisibility(View.INVISIBLE);
                break;
        }
        refreshData();
    }

    private void updateDescriptions(String description) {
        pieChart.setCenterText(description);
        barChart.setDescription(description);
    }

    private void setVisibleChart(int newVisibleChart, boolean refresh) {
        if(txtNoData.getVisibility() == View.GONE || refresh) {
            visibleChart = newVisibleChart;
            switch (newVisibleChart) {
                case PIE_CHART:
                    pieChart.setVisibility(View.VISIBLE);
                    barChart.setVisibility(View.GONE);
                    spiderChart.setVisibility(View.GONE);
                    lineChart.setVisibility(View.GONE);
                    break;
                case BAR_CHART:
                    pieChart.setVisibility(View.GONE);
                    barChart.setVisibility(View.VISIBLE);
                    spiderChart.setVisibility(View.GONE);
                    lineChart.setVisibility(View.GONE);
                    break;
                case SPIDER_CHART:
                    pieChart.setVisibility(View.GONE);
                    barChart.setVisibility(View.GONE);
                    if(mode == SHOW_ACTIVITIES) {
                        spiderChart.setVisibility(View.GONE);
                        lineChart.setVisibility(View.VISIBLE);
                    } else {
                        spiderChart.setVisibility(View.VISIBLE);
                        lineChart.setVisibility(View.GONE);
                    }
                    break;
            }
        }
        if(refresh) {
            refreshData();
        }
    }

    private void showCharts() {
        txtNoData.setVisibility(View.GONE);
        setVisibleChart(visibleChart, false);
    }

    private void hideCharts() {
        txtNoData.setVisibility(View.VISIBLE);
        pieChart.setVisibility(View.GONE);
        barChart.setVisibility(View.GONE);
        spiderChart.setVisibility(View.GONE);
        lineChart.setVisibility(View.GONE);
    }

    private void initData(String[] filenames, ArrayList<Timeline> timelines) {
        //daysToCategoriesMap = new DaysToCategoriesMap(new DayMonthComparator());
        daysToCategoriesMap = new DaysToCategoriesMap();
        CategoriesToActivitiesMap categoryActivityHashMap;
        for(int i = 0; i < filenames.length; i++) {
            String filename = filenames[i];
            categoryActivityHashMap = new CategoriesToActivitiesMap();
            categoryActivityHashMap.init();
            String[] yy_mm_dd = filename.split("\\.");
            if(yy_mm_dd.length == 3) {
                filenames[i] = yy_mm_dd[2] + "." + yy_mm_dd[1];
            }
            daysToCategoriesMap.put(filenames[i], categoryActivityHashMap);
        }

        for(int i = 0; i < filenames.length; i++) {
            categoryActivityHashMap = daysToCategoriesMap.get(filenames[i]);
            for (ActivityBlock ab : timelines.get(i)) {
                Category category = ab.getTask().getCategory();
                HashMap<String, Float> stringDoubleHashMap = categoryActivityHashMap.get(category);
                String activity = ab.getTask().getName();
                if (!stringDoubleHashMap.containsKey(activity)) {
                    stringDoubleHashMap.put(activity, 0f);
                }
                stringDoubleHashMap.put(activity, stringDoubleHashMap.get(activity) +
                        ab.getLengthInMinutes() / 60f);
            }
        }
    }

    private ArrayList<Category> getAllAllowedCategories() {
        ArrayList<Category> categoryArrayList = new ArrayList<>();
        for(Category category : Category.values()) {
            if(switchUncategorized.isChecked() || !category.equals(Category.UNCATEGORIZED)) {
                categoryArrayList.add(category);
            }
        }
        return categoryArrayList;
    }

    private void refreshData() {
        switch(mode) {
            case SHOW_CATEGORIES:
                updateCharts(getAllAllowedCategories());
                break;
            case SHOW_ACTIVITIES:
                updateCharts(filterCategory);
                break;
        }
    }

    private void updateCharts(ArrayList<Category> categories) {
        switch (visibleChart) {
            case PIE_CHART:
                updatePieChart(getPieData(categories));
                break;
            case BAR_CHART:
                updateBarChart(getBarData(categories));
                break;
            case SPIDER_CHART:
                updateSpiderChart(getRadarData(categories));
                break;
        }
    }

    private void updateCharts(Category filterCategory) {
        switch (visibleChart) {
            case PIE_CHART:
                updatePieChart(getPieData(filterCategory));
                break;
            case BAR_CHART:
                updateBarChart(getBarData(filterCategory));
                break;
            case SPIDER_CHART:
                updateLineChart(getLineData(filterCategory));
                break;
        }
    }

    private void updatePieChart(PieData pieData) {
        if(pieData != null) {
            pieData.setValueTextColor(Color.WHITE);
            pieData.setValueTextSize(14);
            pieData.setValueFormatter(mValueFormatter);
            pieChart.setData(pieData);
            pieChart.invalidate();
            showCharts();
        } else {
            pieChart.clear();
            hideCharts();
        }
    }

    private void updateBarChart(BarData barData) {
        if(barData != null) {
            barData.setGroupSpace(0);
            barData.setValueFormatter(mValueFormatter);
            for(DataSet<?> set : barData.getDataSets()) {
                set.setDrawValues(false);
            }
            barChart.setData(barData);
            barChart.invalidate();
            showCharts();
        } else {
            barChart.clear();
            hideCharts();
        }
    }

    private void updateSpiderChart(RadarData radarData) {
        if(radarData != null) {
            radarData.setValueFormatter(mValueFormatter);
            spiderChart.setData(radarData);
            spiderChart.invalidate();
            showCharts();
        } else {
            spiderChart.clear();
            hideCharts();
        }
    }

    private void updateLineChart(LineData lineData) {
        if(lineData != null) {
            lineData.setValueFormatter(mValueFormatter);
            lineChart.setData(lineData);
            lineChart.invalidate();
            showCharts();
        } else {
            spiderChart.clear();
            hideCharts();
        }
    }

    private PieData getPieData(ArrayList<Category> categories) {
        ArrayList<String> categoryNames = new ArrayList<>();
        List<Entry> pieActivityValues = new ArrayList<>();

        int index = 0;
        CategoriesToActivitiesMap categoriesToActivitiesMap = daysToCategoriesMap.joinAll();
        for (Category category : Category.values()) {
            if (categories.contains(category)) {
                float value = categoriesToActivitiesMap.get(category).getTotalDuration();
                if (value > 0) {
                    categoryNames.add(category.toString());
                    pieActivityValues.add(new Entry(value, index));
                    index += 1;
                }
            }
        }
        if(pieActivityValues.size() > 0) {
            PieDataSet pieDataSet = new PieDataSet(pieActivityValues, "Categories");
            ArrayList<Integer> colors2 = new ArrayList<>();
            for(Category category : Category.values()) {
                if(categories.contains(category)) {
                    colors2.add(colors.get(category.ordinal()));
                }
            }
            pieDataSet.setColors(colors2);
            datasetTitles = categoryNames;
            return new PieData(categoryNames, pieDataSet);
        }
        return null;
    }

    private PieData getPieData(Category filterCategory) {
        ArrayList<String> activityNames = new ArrayList<>();
        List<Entry> pieActivityValues = new ArrayList<>();

        CategoriesToActivitiesMap categoriesToActivitiesMap = daysToCategoriesMap.joinAll();
        int index = 0;
        for (Category category : Category.values()) {
            if (category.equals(filterCategory)) {
                for (Map.Entry<String, Float> entry : categoriesToActivitiesMap.get(category)
                        .entrySet()) {
                    activityNames.add(entry.getKey());
                    pieActivityValues.add(new Entry(entry.getValue(), index));
                    index += 1;
                }
            }
        }
        if(pieActivityValues.size() > 0) {
            PieDataSet pieDataSet = new PieDataSet(pieActivityValues, "Activities");
            pieDataSet.setColors(colorActivities);
            datasetTitles = activityNames;
            return new PieData(activityNames, pieDataSet);
        }
        return null;
    }

    private BarData getBarData(ArrayList<Category> categories) {
        ArrayList<String> categoryNames = new ArrayList<>();
        ArrayList<BarDataSet> barDataSets = new ArrayList<>();

        int dayIndex = 0;
        for(Map.Entry<String, CategoriesToActivitiesMap> entry : daysToCategoriesMap.entrySet()) {
            int index = 0;
            int categoriesGreaterThanZero = 0;
            ArrayList<BarEntry> yVals = new ArrayList<>();
            for(Category category : categories) {
                if(!categoryNames.contains(category.toString())){
                    categoryNames.add(category.toString());
                }
                ActivitiesToDurationsMap activitiesToDurationsMap = entry.getValue().get(category);
                float totalDuration = activitiesToDurationsMap.getTotalDuration();
                yVals.add(new BarEntry(totalDuration, index));
                if(totalDuration > 0) {
                    categoriesGreaterThanZero += 1;
                }
                index += 1;
            }
            if(categoriesGreaterThanZero > 0) {
                BarDataSet barDataSet = new BarDataSet(yVals, entry.getKey());
                barDataSet.setColor(colors.get(dayIndex));
                barDataSets.add(barDataSet);
                dayIndex += 1;
            }
        }
        if(barDataSets.size() > 0) {
            datasetTitles = categoryNames;
            return new BarData(categoryNames, barDataSets);
        }
        return null;
    }

    private BarData getBarData(Category filterCategory) {
        HashMap<String, Integer> stringIntegerHashMap = new HashMap<>();
        ArrayList<BarDataSet> barDataSets = new ArrayList<>();

        int activityIndex = 0;
        int dayIndex = 0;
        for(Map.Entry<String, CategoriesToActivitiesMap> entry : daysToCategoriesMap.entrySet()) {
            ArrayList<BarEntry> yVals = new ArrayList<>();
            for(Map.Entry<Category, ActivitiesToDurationsMap> e : entry.getValue().entrySet()) {
                if(filterCategory.equals(e.getKey())) {
                    for(Map.Entry<String, Float> activityDuration : e.getValue().entrySet()) {
                        if(!stringIntegerHashMap.containsKey(activityDuration.getKey())) {
                            stringIntegerHashMap.put(activityDuration.getKey(), activityIndex);
                            activityIndex += 1;
                        }
                        yVals.add(new BarEntry(activityDuration.getValue(),
                                stringIntegerHashMap.get(activityDuration.getKey())));
                    }
                }
            }
            if(yVals.size() > 0) {
                BarDataSet barDataSet = new BarDataSet(yVals, entry.getKey());
                barDataSet.setColor(colors.get(dayIndex));
                barDataSets.add(barDataSet);
                dayIndex += 1;
            }
        }
        if(barDataSets.size() > 0) {
            ArrayList<String> activityNames = new ArrayList<>();
            activityNames.addAll(stringIntegerHashMap.keySet());
            datasetTitles = activityNames;
            return new BarData(activityNames, barDataSets);
        }
        return null;
    }

    private RadarData getRadarData(ArrayList<Category> categories) {
        ArrayList<String> categoryNames = new ArrayList<>();
        ArrayList<RadarDataSet> radarDataSets = new ArrayList<>();
        int dayIndex = 0;
        for(Map.Entry<String, CategoriesToActivitiesMap> entry : daysToCategoriesMap.entrySet()) {
            int categoriesGreaterThanZero = 0;
            ArrayList<Entry> yVals = new ArrayList<>();
            for(Category category : categories) {
                if(!categoryNames.contains(category.toString())){
                    categoryNames.add(category.toString());
                }
                ActivitiesToDurationsMap activitiesToDurationsMap = entry.getValue().get(category);
                float totalDuration = activitiesToDurationsMap.getTotalDuration();
                yVals.add(new BarEntry(totalDuration, dayIndex));
                if(totalDuration > 0) {
                    categoriesGreaterThanZero += 1;
                }
            }
            if(categoriesGreaterThanZero > 0) {
                RadarDataSet radarDataSet = new RadarDataSet(yVals, entry.getKey());
                radarDataSet.setLineWidth(3);
                radarDataSet.setColor(colorActivities.get(dayIndex));
                radarDataSets.add(radarDataSet);
                dayIndex += 1;
            }
        }
        if(radarDataSets.size() > 0) {
            datasetTitles = categoryNames;
            return new RadarData(categoryNames, radarDataSets);
        }
        return null;
    }

    private LineData getLineData(Category filterCategory) {
        ArrayList<String> dayNames = new ArrayList<>();
        ArrayList<LineDataSet> lineDataSets = new ArrayList<>();
        int dayIndex = 0;
        ArrayList<Entry> yVals = new ArrayList<>();
        for(Map.Entry<String, CategoriesToActivitiesMap> entry : daysToCategoriesMap.entrySet()) {
            dayNames.add(entry.getKey());
            for(Map.Entry<Category, ActivitiesToDurationsMap> e : entry.getValue().entrySet()) {
                if(e.getKey() == filterCategory) {
                    yVals.add(new Entry(e.getValue().getTotalDuration(), dayIndex));
                }
            }
            dayIndex += 1;
        }
        LineDataSet lineDataSet = new LineDataSet(yVals, filterCategory.toString());
        lineDataSet.setLineWidth(3);
        lineDataSet.setColor(colorActivities.get(filterCategory.ordinal()));
        lineDataSets.add(lineDataSet);
        if(lineDataSets.size() > 0) {
            datasetTitles = dayNames;
            return new LineData(dayNames, lineDataSets);
        }
        return null;
    }

    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
        if(mode == SHOW_CATEGORIES) {
            String wantedCategory = datasetTitles.get(e.getXIndex());
            Category category = null;
            for (Category c : Category.values()) {
                if (c.toString().equals(wantedCategory)) {
                    category = c;
                }
            }
            switchMode(SHOW_ACTIVITIES, category);
            switch (visibleChart) {
                case PIE_CHART:
                    pieChart.highlightTouch(null);
                    break;
                case BAR_CHART:
                    barChart.highlightTouch(null);
                    break;
                case SPIDER_CHART:
                    spiderChart.highlightTouch(null);
                    break;
            }
        }
    }

    @Override
    public void onNothingSelected() {
    }

    public boolean goBackToCategories() {
        if(mode != SHOW_CATEGORIES) {
            switchMode(SHOW_CATEGORIES, null);
            refreshData();
            return true;
        }
        return false;
    }

    class MyValueFormatter implements ValueFormatter {
        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
            int hourValue = (int) Math.floor(value);
            int minuteValue = (int) Math.round((value - hourValue) * 60.0);
            return hourValue + "h" + String.format("%02d", minuteValue);
        }
    }

    class DayMonthComparator implements Comparator<String> {
        @Override
        public int compare(String lhs, String rhs) {
            if (lhs.equals("current day")) {
                return 1;
            }
            if (rhs.equals("current day")) {
                return -1;
            }
            String[] dd_mm1 = lhs.split("\\.");
            String[] dd_mm2 = rhs.split("\\.");
            if (Integer.valueOf(dd_mm1[1]) > Integer.valueOf(dd_mm2[1])) {
                return 1;
            } else {
                if (Integer.valueOf(dd_mm1[0]) > Integer.valueOf(dd_mm2[0])) {
                    return 1;
                } else {
                    if ((Integer.valueOf(dd_mm1[1]).equals(Integer.valueOf(dd_mm2[1]))) &&
                            (Integer.valueOf(dd_mm1[0]).equals(Integer.valueOf(dd_mm2[0])))) {
                        return 0;
                    } else {
                        return -1;
                    }
                }
            }
        }
    }

    class DaysToCategoriesMap extends TreeMap<String, CategoriesToActivitiesMap> {
        /*
        public DaysToCategoriesMap(Comparator<String> comparator) {
            super(comparator);
        }
        */

        public CategoriesToActivitiesMap joinAll() {
            CategoriesToActivitiesMap categoriesToActivitiesMap = null;
            for (Map.Entry<String, CategoriesToActivitiesMap> entry : entrySet()) {
                categoriesToActivitiesMap = categoriesToActivitiesMap == null ? entry.getValue() :
                        categoriesToActivitiesMap.add(entry.getValue());
            }
            return categoriesToActivitiesMap;
        }
    }

    class CategoriesToActivitiesMap extends HashMap<Category, ActivitiesToDurationsMap> {
        public void init() {
            for(Category category : Category.values()) {
                this.put(category, new ActivitiesToDurationsMap());
            }
        }

        public CategoriesToActivitiesMap add(CategoriesToActivitiesMap other) {
            CategoriesToActivitiesMap categoriesToActivitiesMap = new CategoriesToActivitiesMap();
            for(Category category : Category.values()) {
                categoriesToActivitiesMap.put(category, get(category).add(other.get(category)));
            }
            return categoriesToActivitiesMap;
        }
    }

    class ActivitiesToDurationsMap extends HashMap<String, Float> {
        public float getTotalDuration() {
            float value = 0f;
            for(Map.Entry<String, Float> entry : entrySet()) {
                value += entry.getValue();
            }
            return value;
        }

        public void safeAdd(String key, Float value) {
            if(!containsKey(key)) {
                put(key, 0f);
            }
            put(key, get(key) + value);
        }

        public ActivitiesToDurationsMap add(ActivitiesToDurationsMap other) {
            ActivitiesToDurationsMap activitiesToDurationsMap = new ActivitiesToDurationsMap();
            for(Map.Entry<String, Float> entry : entrySet()) {
                activitiesToDurationsMap.safeAdd(entry.getKey(), entry.getValue());
            }
            for(Map.Entry<String, Float> entry : other.entrySet()) {
                activitiesToDurationsMap.safeAdd(entry.getKey(), entry.getValue());
            }
            return activitiesToDurationsMap;
        }
    }
}
