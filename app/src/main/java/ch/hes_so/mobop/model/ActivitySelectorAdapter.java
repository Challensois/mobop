package ch.hes_so.mobop.model;

import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import ch.hes_so.mobop.NewActivity;
import ch.hes_so.mobop.R;

public class ActivitySelectorAdapter
        extends RecyclerView.Adapter<ActivitySelectorAdapter.ViewHolder> implements Observer {
    private NewActivity context;
    private List<ActivitySelection> activities;

    public ActivitySelectorAdapter(NewActivity parent,
                                   ArrayList<ActivitySelection> activitySelectionList) {
        context = parent;
        activities = activitySelectionList;
        parent.getActivities().addObserver(this);
    }
    @Override
    public ActivitySelectorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_activity, parent, false);
        ViewHolder vh = new ViewHolder(v);
        v.setOnClickListener(vh);
        return vh;
    }

    @Override
    public void onBindViewHolder(ActivitySelectorAdapter.ViewHolder holder, int position) {
        holder.bindActivityBlock(activities.get(position));
        holder.text.setText(holder.as.getName());
        if(holder.as.isSelected()) {
            holder.text.setTextColor(ContextCompat.getColor(context, R.color.turquoise));
            for(Drawable d : holder.text.getCompoundDrawables()) {
                if(d != null) {
                    d.setAlpha(255);
                }
            }
        } else {
            holder.text.setTextColor(ContextCompat.getColor(context, R.color.black));
            for(Drawable d : holder.text.getCompoundDrawables()) {
                if(d != null) {
                    d.setAlpha(0);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return activities.size();
    }

    @Override
    public void update(Observable observable, Object data) {
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public Button text;
        public ActivitySelection as;

        public ViewHolder(View v) {
            super(v);
            text = (Button) v;
        }

        public void bindActivityBlock(ActivitySelection as) {
            this.as = as;
        }

        @Override
        public void onClick(View v) {
            as.setSelected(!as.isSelected());
        }
    }
}
