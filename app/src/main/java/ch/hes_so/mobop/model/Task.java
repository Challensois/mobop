package ch.hes_so.mobop.model;

import java.io.Serializable;

/**
 * A simple wrapper class for the String class.
 * The only change is to compare the string in a case insensitive manner.
 */
public class Task implements Serializable {
    private Category category;
    private String name;
    private static final Task DEFAULT_TASK = new Task(Category.UNCATEGORIZED, "No Activity");

    private static final long serialVersionUID = 14L;

    /**
     * Creates a new activity belonging to a category from a name.
     * @param category The activity category
     * @param name The activity name
     */
    public Task(Category category, String name) {
        this.category = category;
        this.name = name;
    }

    /**
     * Creates a new activity from another activity
     * @param task Another activity
     */
    public Task(Task task) {
        this.category = task.category;
        this.name = task.name;
    }

    /**
     * Returns the category of the activity.
     * @return the category of the activity
     */
    public Category getCategory() {
        return category;
    }

    /**
     * Returns the string name of the activity.
     * @return The string name of the activity
     */
    public String getName() {
        return name;
    }

    /**
     * Compares two activity names in lowercase.
     * @param o an activity
     * @return true if the other activity name is the same (case insensitive) as the current one
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {
            return false;
        } else if(o.getClass() == Task.class) {
            return (((Task)o).category == category) && ((Task) o).name.equals(name);
        }
        return super.equals(o);
    }

    public static String getMinStartingTime() {
        return "00:00";
    }

    public static String getMaxStoppingTime() {
        return "23:59";
    }

    public static Task getDefaultTask() {
        return DEFAULT_TASK;
    }

    public static Task getNewDefaultTask() {
        return new Task(DEFAULT_TASK);
    }

    public boolean isDefaultTask() {
        return this.equals(DEFAULT_TASK);
    }

    /**
     * Returns the name of the activity.
     * @return the name of the activity
     */
    @Override
    public String toString() {
        return name;
    }
}