package ch.hes_so.mobop.model;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Observable;
import java.util.Observer;

import ch.hes_so.mobop.DailyActivities;

/**
 * This class represents a complete day with a chained list of activities.
 */
public class Timeline extends Observable implements Iterable<ActivityBlock>, Serializable {
    private static Timeline instance = null;

    /**
     * The list of activities in a day
     */
    private List<ActivityBlock> activities;

    /**
     * The serializable's version
     */
    private static final long serialVersionUID = 666L;

    /**
     * Creates a new Timeline with only one block of the default activity.
     * This is private so that the only way to get it is through the getInstance method,
     * or the readFromFile method.
     */
    private Timeline() {
        activities = new ArrayList<>();
        activities.add(new ActivityBlock(Task.getDefaultTask(),
                Task.getMinStartingTime(),
                Task.getMaxStoppingTime()));
    }

    /**
     * Gets the singleton instance of this class
     * @return singleton instance of this class
     */
    public static synchronized Timeline getInstance() {
        if(instance == null) {
            instance = new Timeline();
        }
        return instance;
    }

    /**
     * Adds a new activity block in the timeline. It will erase previously-defined activities.
     * Also, if the block immediately before or after him is the same, it will extend to a single
     * block. If the block is completely contained inside another block, it will creates two
     * surrounding blocks of the old activity around the new block.
     * @param newActivity the new activity to be added to the timeline
     */
    public synchronized void addActivity(ActivityBlock newActivity) {
        if(newActivity.getBegin().compareTo(Utils.createTimestampFromHourMin(Task.getMinStartingTime())) < 0) {
            throw new RuntimeException("Invalid starting time!");
        }
        if(newActivity.getEnd().compareTo(Utils.createTimestampFromHourMin(Task.getMaxStoppingTime())) > 0) {
            throw new RuntimeException("Invalid ending time!");
        }

        ListIterator<ActivityBlock> iterator = activities.listIterator();
        while(iterator.hasNext()) {
            ActivityBlock oldActivity = iterator.next();
            // If completely contained in an old block
            if(newActivity.getBegin().compareTo(oldActivity.getBegin()) >= 0 &&
                    newActivity.getEnd().compareTo(oldActivity.getEnd()) <= 0) {
                Timestamp old = oldActivity.getEnd();
                oldActivity.setEnd(newActivity.getBegin());
                iterator.add(newActivity);
                iterator.add(new ActivityBlock(oldActivity.getTask(), newActivity.getEnd(), old));
            // Neither the end nor the beginning is contained inside an old block
            } else if(newActivity.getBegin().compareTo(oldActivity.getBegin()) <= 0
                    && newActivity.getEnd().compareTo(oldActivity.getEnd()) >= 0) {
                oldActivity.setEnd(oldActivity.getBegin());
                iterator.add(newActivity);
            } else {
                // Only the beginning is contained inside an old block
                if(newActivity.getBegin().compareTo(oldActivity.getBegin()) >= 0
                        && newActivity.getBegin().compareTo(oldActivity.getEnd()) <= 0) {
                    oldActivity.setEnd(newActivity.getBegin());
                    iterator.add(newActivity);
                }
                // Only the end is contained inside an old block
                if(newActivity.getEnd().compareTo(oldActivity.getBegin()) >= 0
                        && newActivity.getEnd().compareTo(oldActivity.getEnd()) <= 0) {
                    oldActivity.setBegin(newActivity.getEnd());
                }
            }
        }

        ensureListStability();
    }

    /**
     * Removes the activity at a given index, by replacing it by the default activity
     * @param index the index at which to remove the activity
     */
    public synchronized void removeActivity(int index) {
        ActivityBlock activity = activities.get(index);
        if(!activity.getTask().isDefaultTask()) {
            activity.setTask(Task.getDefaultTask());
            ensureListStability();
        }
    }

    /**
     * Ensures the list stability by removing 0-length activities, and contiguous blocks.
     */
    private synchronized void ensureListStability() {
        //Removes empty activities (0 length)
        ListIterator<ActivityBlock> iterator = activities.listIterator();
        while(iterator.hasNext()) {
            ActivityBlock currentActivity = iterator.next();
            if (currentActivity.getEnd().compareTo(currentActivity.getBegin()) <= 0) {
                iterator.remove();
            }
        }

        //Merges adjacent same activities in a bigger block
        iterator = activities.listIterator();
        ActivityBlock previous = iterator.next();
        while(iterator.hasNext()) {
            ActivityBlock block = iterator.next();
            if(previous.getTask().equals(block.getTask())) {
                previous.setEnd(block.getEnd());
                iterator.remove();
            } else {
                previous = block;
            }
        }
        setChanged();
        notifyObservers();
    }

    /**
     * Returns a copy of the activity blocks.
     * @return a copy of the activity blocks
     */
    public synchronized ArrayList<ActivityBlock> getActivities() {
        ArrayList<ActivityBlock> blocks = new ArrayList<>();
        for(ActivityBlock block : activities) {
            blocks.add(block);
        }
        return blocks;
    }

    /**
     * In order to be able to iterate over this Timeline directly
     * @return the iterator over this timeline's activity blocks.
     */
    @Override
    public Iterator<ActivityBlock> iterator() {
        return activities.iterator();
    }

    /**
     * A block by block string representation of this timeline.
     * @return a block by block string representation of this timeline.
     */
    public String toString() {
        StringBuilder stringRepresentation = new StringBuilder();
        stringRepresentation.append("[");
        String prefix = "";
        for(ActivityBlock block : activities) {
            stringRepresentation.append(prefix);
            prefix = ";";
            stringRepresentation.append(block.toString());
        }
        stringRepresentation.append("]");
        return stringRepresentation.toString();
    }

    /**
     * Serializes the timeline and saves it to a file
     * @param activity the context from which to save the timeline in a file
     */
    public void saveToFile(final DailyActivities activity) {
        Calendar c = Calendar.getInstance();
        final String fileName = TimelineSaver.getFileNameFromDay(c.get(Calendar.YEAR),
                c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        makeSave(activity, fileName);
    }

    /**
     * Serializes the timeline and saves it to a file.
     * This method is only used as test purposes for loading sample data.
     * @param activity
     * @param calendar
     */
    public void saveToFile(final DailyActivities activity, final Calendar calendar) {
        final String fileName = TimelineSaver.getFileNameFromDay(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        makeSave(activity, fileName);
    }

    /**
     * Serializes the timeline and saves it to a file
     * @param activity the context from which to save the timeline in a file
     * @param fileName the name of the file soon-to-be written
     */
    private void makeSave(DailyActivities activity, String fileName) {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = activity.openFileOutput(fileName, Context.MODE_PRIVATE);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(Timeline.this);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if(oos != null) {
                    oos.close();
                }
                if(fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Reads a Timeline serialized in a given file.
     * @param context the context from which to look for the file
     * @param fileName the name of the file to deserialize
     * @return the Timeline object deserialized
     */
    public static Timeline readFromFile(Context context, String fileName) {
        Timeline createResumeForm = null;
        try {
            FileInputStream fileInputStream = context.openFileInput(fileName);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            createResumeForm = (Timeline) objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }
        return createResumeForm;
    }

    /******** This part is a observer-observable design for the views ********/

    /**
     * Adds an observer o
     * @param o the new observer
     */
    public synchronized void subscribe(Observer o) {
        addObserver(o);
    }

    /**
     * Toggles the block's state as de/selected.
     * @param position the position of the block in the timeline
     */
    public synchronized void setSelected(int position) {
        ActivityBlock block = activities.get(position);
        block.setSelected(!block.isSelected());
        setChanged();
        notifyObservers();
    }

    /**
     * Counts the number of selected blocks in the timeline.
     * @return the number of selected blocks int the timeline
     */
    public synchronized int getNumberSelected() {
        int count = 0;
        for (ActivityBlock block : activities) {
            if(block.isSelected()) {
                count += 1;
            }
        }
        return count;
    }

    /**
     * Deletes all selected blocks in the timeline (and replaces them with no activity)
     */
    public synchronized void deleteSelected() {
        for (ActivityBlock block : activities) {
            if(block.isSelected() && !block.getTask().equals(Task.getDefaultTask())) {
                block.setTask(Task.getDefaultTask());
                block.setSelected(false);
            }
        }
        ensureListStability();
        setChanged();
        notifyObservers();
    }

    /**
     * Puts all blocks to a non-selected state.
     */
    public synchronized void resetSelected() {
        for (ActivityBlock block : activities) {
            if(block.isSelected()) {
                block.setSelected(false);
            }
        }
        setChanged();
        notifyObservers();
    }
}
