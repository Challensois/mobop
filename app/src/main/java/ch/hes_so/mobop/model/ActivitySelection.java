package ch.hes_so.mobop.model;

import java.util.Observable;

public class ActivitySelection extends Observable {
    private final Category category;
    private final String name;
    private boolean selected;

    public ActivitySelection(Category category, String name) {
        this.category = category;
        this.name = name;
        this.selected = false;
    }

    public boolean equals(Object o) {
        if(o.getClass() == ActivitySelection.class) {
            ActivitySelection other = (ActivitySelection) o;
            return category.equals(other.category) && name.equals(other.name);
        }
        return false;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
        setChanged();
        notifyObservers();
    }

    public Category getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public boolean isSelected() {
        return selected;
    }
}