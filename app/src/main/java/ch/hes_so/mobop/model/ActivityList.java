package ch.hes_so.mobop.model;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class ActivityList {
    private final static String ACTIVITY_SHARED_PREFERENCES_FILE_NAME = "activity_list";
    private static HashMap<Category, List<String>> activityList;
    private final static int ACTIVITY_NAME_MAX_LENGTH = 20;

    public static List<String> getActivityListForCategory(Context context, Category category) {
        if (activityList == null) {
            activityList = new HashMap<>();
            loadActivityList(context);
        }
        return activityList.get(category);
    }

    private static void loadActivityList(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                ACTIVITY_SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        for(Category category : Category.values()) {
            String categoryName = category.toString();
            Set<String> defaultActivities = new HashSet<>();
            defaultActivities.add(category.getDefaultActivity());
            if(!sharedPreferences.contains(categoryName)) {
                List<String> activities = new LinkedList<>();
                activities.addAll(defaultActivities);
                activityList.put(category, activities);
                editor.putStringSet(categoryName, defaultActivities);
            } else {
                Set<String> set = sharedPreferences.getStringSet(categoryName, defaultActivities);
                List<String> activities = new LinkedList<>();
                activities.addAll(set);
                activityList.put(category, activities);
            }
        }
        editor.apply();
    }

    public static void saveActivityList(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                ACTIVITY_SHARED_PREFERENCES_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        for(Category category : Category.values()) {
            String categoryName = category.toString();
            List<String> activities = activityList.get(category);
            Set<String> set = new HashSet<>();
            set.addAll(activities);
            editor.putStringSet(categoryName, set);
        }
        editor.apply();
    }

    public static Result addActivityToCategory(Category category, String activityName) {
        Result result = isValid(activityName);
        switch (result) {
            case OK:
                break;
            case TOO_LONG:
                return result;
            case EMPTY:
                return result;
        }
        List<String> activities = activityList.get(category);
        Set<String> set = new HashSet<>();
        set.addAll(activities);
        boolean success = set.add(activityName);
        if(success) {
            activities.add(activityName);
            return Result.OK;
        }
        return Result.ALREADY_EXISTS;
    }

    public static boolean removeActivityFromCategory(Category category, String activityName) {
        List<String> activities = activityList.get(category);
        Set<String> set = new HashSet<>();
        set.addAll(activities);
        boolean success = set.remove(activityName);
        if(success) {
            activities.remove(activityName);
        }
        return success;
    }

    public static Result isValid(String activityName) {
        activityName = activityName.trim();
        if(activityName.length() > ACTIVITY_NAME_MAX_LENGTH) {
            return Result.TOO_LONG;
        }
        if(activityName.equals("")) {
            return Result.EMPTY;
        }
        return Result.OK;
    }

    public static int getActivityNameMaxLength() {
        return ACTIVITY_NAME_MAX_LENGTH;
    }

    public enum Result {
        OK,
        TOO_LONG,
        EMPTY,
        ALREADY_EXISTS
    }
}
