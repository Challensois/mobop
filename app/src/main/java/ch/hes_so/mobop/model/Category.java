package ch.hes_so.mobop.model;

public enum Category {
    PERSONAL("Personal", "Eat"),
    WORK("Work", "Work"),
    SPORT("Sport", "Rugby"),
    TRANSPORT("Transportation", "Train"),
    UNCATEGORIZED("Uncategorized", "No Activity");

    private String title;
    private String defaultActivity;

    Category(String title, String defaultActivity) {
        this.title = title;
        this.defaultActivity = defaultActivity;
    }

    public String getDefaultActivity() {
        return defaultActivity;
    }

    @Override
    public String toString() {
        return this.title;
    }
}
