package ch.hes_so.mobop.model;

import java.util.Calendar;
import java.util.Date;

public class TimelineSaver {
    public final static String INTENT_FILENAME = "filename";
    private final static String FILE_EXTENSION = ".json";

    public static String getFileNameFromDay(int year, int month, int dayOfMonth) {
        return year + "_" + month + "_" + dayOfMonth + FILE_EXTENSION;
    }

    public static String[] getYY_MM_DD_FromFileName(String filename) {
        String yy_mm_dd_string = filename;
        if(filename.endsWith(FILE_EXTENSION)) {
            yy_mm_dd_string = yy_mm_dd_string.substring(0, filename.length() - FILE_EXTENSION.length());
        }
        return yy_mm_dd_string.split("_");
    }

    public static String getFileNameFromDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return getFileNameFromDay(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
    }
}
