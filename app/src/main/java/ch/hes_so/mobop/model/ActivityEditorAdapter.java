package ch.hes_so.mobop.model;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.TreeSet;

import ch.hes_so.mobop.R;
import ch.hes_so.mobop.SetActivitiesActivity;
import ch.hes_so.mobop.TimeManagementActivity;

public class ActivityEditorAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    private TimeManagementActivity context;

    private ArrayList<String> mData = new ArrayList<>();
    private TreeSet<Integer> sectionHeader = new TreeSet<>();
    private Integer newItemPosition = null;
    private String newItemValue = null;

    private final static int LIST_ITEM = 0;
    private final static int LIST_SEPARATOR = 1;
    private final static int LIST_ITEM_NEW = 2;

    private LayoutInflater mInflater;

    public ActivityEditorAdapter(Context context) {
        this.context = (TimeManagementActivity) context;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItem(final String item) {
        mData.add(item);
    }

    private void addItem(int position, final String item) {
        if(position < getItemCount()) {
            mData.add(position, item);
        } else {
            mData.add(item);
        }
        TreeSet<Integer> newSectionHeader = new TreeSet<>();
        for(Integer value : sectionHeader) {
            if(value >= position) {
                newSectionHeader.add(value + 1);
            } else {
                newSectionHeader.add(value);
            }
        }
        sectionHeader = newSectionHeader;
        notifyItemInserted(position);
    }

    private void removeItem(int position) {
        mData.remove(position);
        TreeSet<Integer> newSectionHeader = new TreeSet<>();
        for(Integer value : sectionHeader) {
            if(value > position) {
                newSectionHeader.add(value - 1);
            } else {
                newSectionHeader.add(value);
            }
        }
        sectionHeader = newSectionHeader;
        notifyItemRemoved(position);
    }

    public void addSectionHeaderItem(final String item) {
        mData.add(item);
        sectionHeader.add(mData.size() - 1);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case LIST_SEPARATOR:
                v = mInflater.inflate(R.layout.list_item_category, parent, false);
                return new CategoryHolder(v, this);
            case LIST_ITEM:
                v = mInflater.inflate(R.layout.list_item_activity_item, parent, false);
                return new ActivityHolder(v, this);
            case LIST_ITEM_NEW:
                v = mInflater.inflate(R.layout.list_item_activity_item_new, parent, false);
                return new NewActivityHolder(v, this);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder.getClass() == CategoryHolder.class) {
            String categoryName = mData.get(position);
            CategoryHolder ch = ((CategoryHolder)holder);
            Bitmap bm = BitmapFactory.decodeResource(context.getResources(),
                    context.getImage(categoryName));
            ch.categoryImage.setImageBitmap(bm);
            ch.category.setText(categoryName);
            ch.btnAdd.setVisibility(categoryName.equals(Category.UNCATEGORIZED.toString()) ?
                    View.INVISIBLE : View.VISIBLE);
        } else if(holder.getClass() == ActivityHolder.class) {
            ActivityHolder ah = (ActivityHolder) holder;
            String activityName = mData.get(position);
            ah.activity.setText(activityName);
            Task task = new Task(getContainingCategory(position), activityName);
            ah.btnDelete.setVisibility(task.isDefaultTask() ? View.INVISIBLE : View.VISIBLE);
        } else {
            NewActivityHolder nah = (NewActivityHolder) holder;
            if(newItemValue != null && !nah.activityName.getText().toString().equals(newItemValue)) {
                nah.activityName.setText(newItemValue);
            }
            nah.activityName.requestFocus();
            InputMethodManager imm = (InputMethodManager)
                    context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(nah.activityName, InputMethodManager.SHOW_IMPLICIT);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,
                    InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public int getItemViewType(int position) {
        return isEditModeOn() && position == newItemPosition ? LIST_ITEM_NEW :
                sectionHeader.contains(position) ? LIST_SEPARATOR : LIST_ITEM;
    }

    public Category getContainingCategory(int position) {
        int categoryNumber = -1;
        for(int i = 0; i <= position; i++) {
            if (getItemViewType(i) == LIST_SEPARATOR) {
                categoryNumber += 1;
            }
        }
        return Category.values()[categoryNumber];
    }

    public int getInsertionIndex(int position) {
        Integer lastItemInCategory = null;
        for(int i = position + 1; i <= getItemCount(); i++) {
            if (getItemViewType(i) == LIST_SEPARATOR) {
                lastItemInCategory = i - 1;
                break;
            }
        }
        return lastItemInCategory == null ? getItemCount() - 1 : lastItemInCategory;
    }

    public boolean isEditModeOn() {
        return newItemPosition != null;
    }

    private void warnEdition() {
        Toast.makeText(context, "Finish editing the current activity first!",
                Toast.LENGTH_LONG).show();
    }

    public boolean removeEditItem() {
        if(isEditModeOn()) {
            removeItem(newItemPosition);
            newItemPosition = null;
            newItemValue = null;
            return true;
        } else {
            return false;
        }
    }

    private boolean verifyAddItem(int position, EditText activityNameText) {
        String activityName = activityNameText.getText().toString().trim();
        activityNameText.setText(activityName);
        Category category = getContainingCategory(position);
        ActivityList.Result result = ActivityList.addActivityToCategory(category, activityName);
        switch (result) {
            case OK:
                mData.set(position, activityName);
                return true;
            case TOO_LONG:
                Toast.makeText(context, "Activity name is too long! (max " + ActivityList.
                                getActivityNameMaxLength() + " characters)",
                        Toast.LENGTH_SHORT).show();
                break;
            case EMPTY:
                Toast.makeText(context, "Activity name is empty!",
                        Toast.LENGTH_SHORT).show();
                break;
            case ALREADY_EXISTS:
                Toast.makeText(context, "Activity \"" + activityName + "\" already exists in " +
                                "category " + category + "!", Toast.LENGTH_SHORT).show();
                break;
        }
        return false;
    }

    private void askRemoveItem(final int position) {
        final String activityName = mData.get(position);
        final Category category = getContainingCategory(position);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Delete Activity");
        builder.setMessage("Are you sure you want to delete \"" + activityName +
                "\" from " + category + "?");
        builder.setIcon(android.R.drawable.ic_dialog_alert);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (ActivityList.removeActivityFromCategory(category, activityName)) {
                    removeItem(position);
                } else {
                    Toast.makeText(context, "Error removing \"" + activityName +
                            "\" from " + category + "!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void scrollToPositionWithOffset(int position, int offset) {
        ((SetActivitiesActivity)context).scrollToPositionWithOffset(position, offset);
    }

    public Integer getEditItem() {
        return newItemPosition;
    }

    public String getEditItemValue() {
        return newItemValue;
    }

    public void setEditItem(int positionEditedItem, String valueEditedItem) {
        if(!isEditModeOn()) {
            addItem(positionEditedItem, "");
            newItemPosition = positionEditedItem;
            newItemValue = valueEditedItem;
        }
    }

    public static class ActivityHolder extends RecyclerView.ViewHolder {
        public TextView activity;
        public ImageButton btnDelete;

        public ActivityHolder(View v, final ActivityEditorAdapter container) {
            super(v);
            activity = (TextView) v.findViewById(R.id.txt_item_item);
            btnDelete = (ImageButton) v.findViewById(R.id.btn_delete_activity_from_category);
            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!container.isEditModeOn()) {
                        container.askRemoveItem(getAdapterPosition());
                    } else {
                        container.warnEdition();
                    }
                }
            });
        }
    }

    public static class NewActivityHolder extends RecyclerView.ViewHolder {
        public EditText activityName;
        public ImageButton btnValid;

        public NewActivityHolder(View v, final ActivityEditorAdapter container) {
            super(v);
            activityName = (EditText) v.findViewById(R.id.input_activity);
            activityName.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    container.newItemValue = s.toString();
                }
            });
            activityName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus) {
                        InputMethodManager iim = (InputMethodManager) container.context
                                .getSystemService(Context.INPUT_METHOD_SERVICE);
                        iim.hideSoftInputFromWindow(activityName.getWindowToken(), 0);
                    }
                }
            });
            btnValid = (ImageButton) v.findViewById(R.id.btn_validate);
            btnValid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (container.verifyAddItem(getAdapterPosition(), activityName)) {
                        activityName.setText("");
                        container.notifyItemChanged(getAdapterPosition());
                        container.newItemPosition = null;
                        InputMethodManager iim = (InputMethodManager) container.context
                                .getSystemService(Context.INPUT_METHOD_SERVICE);
                        iim.hideSoftInputFromWindow(activityName.getWindowToken(), 0);
                    }
                }
            });
        }
    }

    public static class CategoryHolder extends RecyclerView.ViewHolder {
        private final ActivityEditorAdapter container;
        public ImageView categoryImage;
        public TextView category;
        public ImageButton btnAdd;

        public CategoryHolder(View v, final ActivityEditorAdapter container) {
            super(v);
            this.container = container;
            categoryImage = (ImageView) v.findViewById(R.id.cat_image);
            category = (TextView) v.findViewById(R.id.txt_item_separator);
            btnAdd = (ImageButton) v.findViewById(R.id.btn_add_activity_to_category);
            btnAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!container.isEditModeOn()) {
                        ActivityEditorAdapter container = CategoryHolder.this.container;
                        int position = getAdapterPosition();
                        final int nextItemPosition = container.getInsertionIndex(position);
                        container.addItem(nextItemPosition + 1, "");
                        container.newItemPosition = nextItemPosition + 1;
                        container.scrollToPositionWithOffset(nextItemPosition + 1, 20);
                    } else {
                        container.warnEdition();
                    }
                }
            });
        }
    }
}