package ch.hes_so.mobop.model;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import ch.hes_so.mobop.DailyActivities;
import ch.hes_so.mobop.R;

public class ActivityBlockAdapter extends RecyclerView.Adapter<ActivityBlockAdapter.ViewHolder> implements Observer {
    private DailyActivities context;
    private ArrayList<ActivityBlock> activities;

    public ActivityBlockAdapter(DailyActivities parent) {
        context = parent;
        activities = Timeline.getInstance().getActivities();
        Timeline.getInstance().addObserver(this);
    }

    public ActivityBlockAdapter(DailyActivities parent, Timeline savedTimeline) {
        context = parent;
        Timeline timeline = Timeline.getInstance();
        for(ActivityBlock activityBlock : savedTimeline) {
            timeline.addActivity(activityBlock);
        }
        activities = Timeline.getInstance().getActivities();
        Timeline.getInstance().addObserver(this);
    }

    @Override
    public void update(Observable observable, Object data) {
        Log.d("update", "timeline update");
        if(observable instanceof Timeline) {
            activities = Timeline.getInstance().getActivities();
            notifyDataSetChanged();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnLongClickListener {
        public View view;
        public ImageView cat_img;
        public TextView task, begin, end;
        public ImageView edit_img;
        public int position;

        public ViewHolder(View v) {
            super(v);
            view = v;
            cat_img = (ImageView) v.findViewById(R.id.cat_image);
            task = (TextView) v.findViewById(R.id.txt_task);
            begin = (TextView) v.findViewById(R.id.txt_begin);
            end = (TextView) v.findViewById(R.id.txt_end);
            edit_img = (ImageView) v.findViewById(R.id.edit_image);
        }

        public void bindActivityBlock(int position) {
            this.position = position;
            ActivityBlock block = Timeline.getInstance().getActivities().get(position);
            Bitmap bm = BitmapFactory.decodeResource(context.getResources(),
                    context.getImage(block.getTask().getCategory()));
            cat_img.setImageBitmap(bm);
            task.setText(block.getTask().getName());
            begin.setText(block.getBeginningString());
            end.setText(block.getEndingString());
        }

        @Override
        public void onClick(View v) {
            if (context.isEditModeOn()) {
                Timeline.getInstance().setSelected(position);
            } else {
                context.transmitClickEvent(position);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if (context.isEditModeOn()) {
                return false;
            } else {
                Timeline.getInstance().setSelected(position);
                context.startEditMode();
                return true;
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_activity_block, parent, false);
        ViewHolder vh = new ViewHolder(v);
        v.setOnClickListener(vh);
        v.setOnLongClickListener(vh);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ActivityBlock currentBlock = activities.get(position);

        holder.view.setLongClickable(true);
        holder.bindActivityBlock(position);

        if(!context.isEditModeOn()) {
            holder.edit_img.setVisibility(View.VISIBLE);
        } else {
            holder.edit_img.setVisibility(View.INVISIBLE);
        }

        int height = currentBlock.getLengthInMinutes();
        if(height < holder.view.getMinimumHeight())
            height = holder.view.getMinimumHeight();
        if(currentBlock.getTask().equals(Task.getDefaultTask())) {
            height = holder.view.getMinimumHeight();
            if(activities.size() != 1) {
                holder.view.setBackgroundColor(ContextCompat.getColor(context,
                        android.R.color.transparent));
                holder.task.setText("");
                holder.edit_img.setVisibility(View.GONE);
            }
        }
        holder.view.setLayoutParams(new ViewGroup.LayoutParams(holder.view.getLayoutParams().width, height));

        if(currentBlock.isSelected()) {
            holder.view.setBackgroundColor(ContextCompat.getColor(context, R.color.turquoise_pressed));
        } else {
            holder.view.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
        }
    }

    @Override
    public int getItemCount() {
        return activities.size();
    }
}