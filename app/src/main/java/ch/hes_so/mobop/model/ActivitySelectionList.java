package ch.hes_so.mobop.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

public class ActivitySelectionList extends Observable implements Observer,
        Iterable<ActivitySelection> {

    private ArrayList<ActivitySelection> activities;

    public ActivitySelectionList() {
        activities = new ArrayList<>();
    }

    public void add(ActivitySelection temp) {
        activities.add(temp);
        temp.addObserver(this);
    }

    public int getSize() {
        return activities.size();
    }

    public ActivitySelection get(int i) {
        return activities.get(i);
    }

    public Iterator<ActivitySelection> iterator() {
        return activities.iterator();
    }

    @Override
    public void update(Observable observable, Object data) {
        ActivitySelection as = (ActivitySelection) observable;
        if(as.isSelected()) {
            for(ActivitySelection activitySelection : activities) {
                if(!activitySelection.equals(as)) {
                    activitySelection.setSelected(false);
                }
            }
        }
        setChanged();
        notifyObservers();
    }
}
