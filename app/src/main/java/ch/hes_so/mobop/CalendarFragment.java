package ch.hes_so.mobop;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.squareup.timessquare.CalendarPickerView;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import ch.hes_so.mobop.model.TimelineSaver;

public class CalendarFragment extends Fragment {
    private Button btnViewStatistics;
    private LinkedList<Date> dates;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_calendar, container, false);

        btnViewStatistics = (Button) rootView.findViewById(R.id.btn_view_statistics);

        if(dateHasRecordedActivity(Calendar.getInstance().getTime())) {
            enableButton();
        } else {
            disableButton();
        }

        dates = new LinkedList<>();
        final CalendarPickerView calendarPickerView = (CalendarPickerView)
                rootView.findViewById(R.id.calendar_view);
        initializeCalendar(calendarPickerView);
        calendarPickerView.setOnDateSelectedListener(new CalendarPickerView.OnDateSelectedListener() {
            @Override
            public void onDateSelected(Date date) {
                dates.clear();
                for (Date d : calendarPickerView.getSelectedDates()) {
                    if (dateHasRecordedActivity(d)) {
                        dates.add(d);
                    }
                }
                if (dates.size() > 0) {
                    enableButton();
                } else {
                    disableButton();
                }
            }

            @Override
            public void onDateUnselected(Date date) {
            }
        });

        return rootView;
    }

    private void initializeCalendar(CalendarPickerView calendarPickerView) {
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        int month = calendar.get(Calendar.MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.YEAR, -1);
        Date minDate = calendar.getTime();
        calendar.add(Calendar.YEAR, 1);
        while(month == calendar.get(Calendar.MONTH)) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        Date maxDate = calendar.getTime();
        calendarPickerView.init(minDate, maxDate).withSelectedDate(today)
                .inMode(CalendarPickerView.SelectionMode.RANGE);
        calendarPickerView.highlightDates(getDatesWithRecordedActivity());
    }

    private List<Date> getDatesWithRecordedActivity() {
        List<Date> dates = new LinkedList<>();
        Calendar calendar = Calendar.getInstance();
        Date firstDate = calendar.getTime();
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1);
        while (!calendar.getTime().equals(firstDate)) {
            Date d = calendar.getTime();
            if(dateHasRecordedActivity(d)) {
                dates.add(d);
                Log.d("Day", d.toString());
            }
            calendar.add(Calendar.DAY_OF_YEAR, 1);
        }
        return dates;
    }

    private boolean dateHasRecordedActivity(Date d) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(d);
        return getActivity().getBaseContext().getFileStreamPath(TimelineSaver.getFileNameFromDay(
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH))).exists();
    }

    private void disableButton() {
        btnViewStatistics.setEnabled(false);
        btnViewStatistics.setText(getString(R.string.no_data));
        btnViewStatistics.setTextColor(ContextCompat.getColor(getActivity(),
                R.color.black));
        btnViewStatistics.setOnClickListener(null);
    }

    private void enableButton() {
        btnViewStatistics.setEnabled(true);
        btnViewStatistics.setText(getString(R.string.btn_view_statistics));
        btnViewStatistics.setTextColor(ContextCompat.getColor(getActivity(),
                R.color.white));
        btnViewStatistics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), StatisticsActivity.class);
                intent.putExtra(TimelineSaver.INTENT_FILENAME, dates.size());
                for(int i = 0; i < dates.size(); i++) {
                    intent.putExtra(TimelineSaver.INTENT_FILENAME + i, dates.get(i));
                }
                startActivity(intent);
            }
        });
    }
}
